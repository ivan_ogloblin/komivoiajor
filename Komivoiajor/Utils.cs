﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace Komivoiajor
{
    public static class Utils
    {
        private static int seed;
        private static Random rng;

        public static void Seed(int s)
        {
            seed = s;
            rng = new Random(s);
        }

        public static int Seed()
        {
            return seed;
        }

        static Utils()
        {
            Seed((int)DateTime.Now.Ticks);

            rng = new Random(seed);
        }

        public static double Rand()
        {
            return rng.NextDouble();
        }

        public static int Rand(int max)
        {
            return (int)(rng.NextDouble() * max);
        }

        public static float Rand(float max)
        {
            return (float)(rng.NextDouble() * max);
        }

        public static double Rand(double max)
        {
            return rng.NextDouble() * max;
        }

        public static int Rand(int min, int max)
        {
            return (int)(rng.NextDouble() * (max - min) + min);
        }

        public static float Rand(float min, float max)
        {
            return (float)(rng.NextDouble() * (max - min) + min);
        }

        public static double Rand(double min, double max)
        {
            return rng.NextDouble() * (max - min) + min;
        }

        public static float Lerp(float val, float x1, float x2, float y1, float y2)
        {
            if (val == x1)
            {
                return y1;
            }

            if (val == x2)
            {
                return y2;
            }

            return (y2 - y1) / (x2 - x1) * (val - x1) + y1;
        }

        public static double Lerp(double val, double x1, double x2, double y1, double y2)
        {
            if (val == x1)
            {
                return y1;
            }

            if (val == x2)
            {
                return y2;
            }

            return (y2 - y1) / (x2 - x1) * (val - x1) + y1;
        }

        public static DMatrix ToAdjacency(List<City> listCities)
        {
            DMatrix res = new DMatrix(listCities.Count);

            for (int i = 0; i < listCities.Count; i++)
            {
                int countFriend = 0;
                for (int j = 0; j < listCities.Count; j++)
                {
                    if (i != j)
                    {
                        res[i, countFriend] += Math.Sqrt(Math.Pow((listCities[i].x - listCities[j].x), 2) + Math.Pow((listCities[i].y - listCities[j].y), 2)); ;
                    }
                    countFriend++;
                }
            }
                   
            return res;
        }

        public static List<City> FrontBranch(DMatrix dMatrix, List<City> listCitys)
        {
            List<City> path = new List<City>();
            List<City> pathElse = new List<City>();

            /*for (int i = 1; i < listCitys.Count; i++)
                pathElse.Add(listCitys[i]);*/

            path.Add(listCitys[0]);
            for (int i = 1; i < listCitys.Count; i++)
            {
                pathElse.Add(listCitys[i]);
            }
            int ssa = 0;
            while (pathElse.Count != 0)
            {
                double width = -1;
                City city = null;
                Dictionary<City, double> dictionaryCity = new Dictionary<City, double>();
                for (int i = 0; i < pathElse.Count; i++)
                {
                    double widthToAdd = ListPath(dMatrix, listCitys, path, pathElse[i]);
                    /*if (widthTmp < width || width == -1)
                    {
                        width = widthTmp;
                        city = pathElse[i];
                    }
                    if (widthTmp == width)
                    {
                        List<City> pathElse = new List<City>();
                    }*/
                    dictionaryCity.Add(pathElse[i], widthToAdd);

                    //доделать если равно
                }
                double widthTmp = -1;
                foreach(KeyValuePair<City, double> kvp in dictionaryCity)
                {
                    if(widthTmp < kvp.Value)
                    {
                        widthTmp = kvp.Value;
                    }
                }
                //Если совпали пути по длине
                List<City> pathElseTmp = new List<City>();
                List<City> pathTmp = new List<City>();
                double widthIfEqual = -1;
                for(int i=0; i < dictionaryCity.Count; i++)
                {
                    if (dictionaryCity[pathElse[i]] == widthTmp)
                    {
                        widthIfEqual = WitdhForPath()
                            }
                }

                path.Add(city);
                pathElse.Remove(city);
            }

            return path;
        }

        public double WitdhForPath(DMatrix dMatrix, List<City> listCitys, List<City> path, City city)
        {

        }

        private static double ListPath(DMatrix dMatrix, List<City> listCitys, List<City> path, City city)
        {
            double shortWidth = 0;
            //маршрут, который уже определён
            List<City> pathTmp = new List<City>();
            foreach(City cityTmp in path)
            {
                pathTmp.Add(cityTmp);
            }
            pathTmp.Add(city);

            //Суммируем всю известную длинну
            for (int i = 1; i < pathTmp.Count; i++)
                shortWidth += Math.Sqrt(Math.Pow((pathTmp[i - 1].x -pathTmp[i].x), 2) + Math.Pow((pathTmp[i - 1].y -pathTmp[i].y), 2));
           
            
            //создаём новый список для того чтобы найти только короткий путь
            List<City> pathShortWidth = new List<City>();
            foreach (City cityTmp in pathTmp)
            {
                pathShortWidth.Add(cityTmp);
            }
            //находим только самую короткую длину
            while (pathShortWidth.Count < listCitys.Count) {
                double shortWidthTmp = -1;
                int index = -1;
                for (int i = 0; i < listCitys.Count; i++)
                {
                    foreach (City cityTmp in pathTmp)
                    {
                        if (i != cityTmp.index)
                        {
                            if (dMatrix[cityTmp.index, i] < shortWidthTmp || shortWidthTmp == -1)
                            {
                                shortWidthTmp = dMatrix[cityTmp.index, i];
                                index = i;
                            }
                            
                        }

                    }
                }
                shortWidth += shortWidthTmp;
                pathShortWidth.Add(listCitys[index]);
            }
            shortWidth += dMatrix[pathShortWidth[pathShortWidth.Count - 1].index, pathShortWidth[0].index];


            double shortWidthRow = 0;
            //находим короткую длину по строке
            for (int i = 0; i < listCitys.Count; i++)
            {
                bool exist = false;
                //foreach (City cityTmp in pathTmp)
                for(int j = 0; j < pathTmp.Count -1; j++)
                {
                    if (i == pathTmp[j].index && (i < pathTmp.Count - 1))
                    {
                        shortWidthRow += dMatrix[i, pathTmp[j + 1].index];
                        exist = true;
                    }
                }
                if (!exist)

                {
                    double min = -1;
                    int conflict = 0;
                    for (int j = 0; j < listCitys.Count; j++)
                    {
                        if ((dMatrix[i, j] < min || min == -1) && dMatrix[i, j] != 0)
                        {
                            min = dMatrix[i, j];
                            //if (dMatrix[i, j] == min)
                            //    conflict++;
                            //Доделать конфликт
                        }
                    }
                    shortWidthRow += min;
                }
            }
            //числа получаются больше!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            double shortWidthCol = 0;
            //находим короткую длину по Столбцам
            for (int i = 0; i < listCitys.Count; i++)
            {
                bool exist = false;
                //foreach (City cityTmp in pathTmp)
                for (int j = 1; j < pathTmp.Count; j++)
                {
                    if (i == pathTmp[j].index && (i != 0))
                    {
                        shortWidthRow += dMatrix[pathTmp[j - 1].index, i];
                    }
                }
                if(!exist)
                {
                    double min = -1;
                    int conflict = 0;
                    for (int j = 0; j < listCitys.Count; j++)
                    {
                        if ((dMatrix[j, i] < min || min == -1) && dMatrix[j, i] != 0)
                        {
                            min = dMatrix[j, i];
                            //if (dMatrix[j, i] == min)
                            //    conflict++;
                            //Доделать конфликт
                        }
                    }
                    shortWidthCol += min;
                }
            }

            double res = -1;

            if (shortWidth > shortWidthRow)
                res = shortWidth;
            else
                res = shortWidthRow;

            if (shortWidthRow > shortWidthCol)
                return res;
            else
                return shortWidthCol;
        }
    }
}
