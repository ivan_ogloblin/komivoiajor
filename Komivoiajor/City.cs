﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Komivoiajor
{
    public class City
    {
        public int x;
        public int y;
        public int index;
        public City()
        {
            x = 0;
            y = 0;
            index = -1;
        }
        public City(int x, int y, int index)
        {
            this.x = x;
            this.y = y;
            this.index = index;
        }
    }
}
